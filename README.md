# Bilibili Popup for Chrome

Inspired by https://www.v2ex.com/t/540394#r_6965765

## Screenshot

![](./screenshot.jpg)

## Usage

1. download or clone repository
2. goto chrome://extensions
3. switch "Developer mode" at right top corner on
4. "Load unpacked"
5. select `extension/` folder
